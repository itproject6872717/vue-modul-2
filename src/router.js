import {createRouter, createWebHashHistory} from "vue-router";
import trips_list from "@/components/trips_list.vue";
import trip_search from "@/components/trip_search.vue";
import create_trip from "@/components/create_trip.vue";
import trip_page from "@/components/trip_page.vue";
const routes = [
    { path: '/', component: trips_list },
    { path: '/search', component: trip_search },
    { path: '/create', component: create_trip },
    { path: '/trip/:id', component: trip_page },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes, // short for `routes: routes`
})

export default router;