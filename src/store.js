import { createStore } from 'vuex'

// Create a new store instance.
import DbOperations from "@/database-helper";
const collectionTitle = "trips";
const collectionDB = new DbOperations(collectionTitle);
export default createStore({
    state: () => ({
        [collectionTitle]: [],
        loading: false,
        error: null,
    }),

    getters: {
        isLoading: (state) => state.loading,
        hasError: (state) => state.error,

        getItemsList: (state) => state[collectionTitle],
        getItemById: (state) => (itemId) =>
            state[collectionTitle].find((item) => item.id == itemId),
    },
    mutations: {
        setItemsList(state, itemsList) {
            state[collectionTitle] = itemsList;
        },
        addItem(state, item) {
            state[collectionTitle].push(item);
        },
        deleteItem(state, deleteItemId) {
            state[collectionTitle] = state[collectionTitle].filter(
                (item) => item.id !== deleteItemId
            );
        },
        setLoading(state, value) {
            state.loading = value;
        },
        setError(state, error) {
            state.error = error;
        },
    },

    actions: {
        loadList({ commit }) {
            commit("setError", null);
            commit("setLoading", true);
            collectionDB
                .loadList()
                .then((list) => {
                    commit("setItemsList", list);
                })
                .catch((error) => {
                    commit("setError", error);
                })
                .finally(() => {
                    commit("setLoading", false);
                });
        },
        addItem({ commit, dispatch }, item) {
            commit("setError", null);
            commit("setLoading", true);
            collectionDB
                .add(item)
                .then(() => {
                    dispatch("loadList");
                })
                .catch((error) => {
                    commit("setError", error);
                })
                .finally(() => {
                    commit("setLoading", false);
                });
        },

        deleteItem({ commit, dispatch }, itemId) {
            commit("setError", null);
            commit("setLoading", true);

            collectionDB
                .delete(itemId)
                .then(() => {
                    dispatch("loadList");
                })
                .catch((error) => {
                    commit("setError", error);
                })
                .finally(() => {
                    commit("setLoading", false);
                });
        },
    },
});
