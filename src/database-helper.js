import {firebaseDB} from "@/firebase_config";
import {doc, collection, getDocs, addDoc, deleteDoc} from "firebase/firestore/lite";

class DbOperations {
    constructor(collectionTitle) {
        this.dbCollection = collection(firebaseDB, `${collectionTitle}`);
    }

    loadList() {// ДІСТАЄ ВСІ поїздки
        return new Promise((resolve, reject) => {
            getDocs(this.dbCollection)
                .then((querySnapshot) => {
                    const list = [];
                    querySnapshot.docs.forEach((doc) => {
                        list.push({
                            id: doc.id,
                            ...doc.data(),
                        });
                    });
                    resolve(list);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    add(item) { //Додає новий об'єкт
        return new Promise((resolve, reject) => {
            addDoc(this.dbCollection, item)
                .then(() => {
                    resolve(true);
                })
                .catch((error) => {
                    console.error("Error adding document:", error);
                    reject(error);
                });
        });
    }

    delete(id) { //Видаляє об'єкт
        return new Promise((resolve, reject) => {
            deleteDoc(doc(this.dbCollection, id))
                .then(() => {
                    resolve(true);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

export default DbOperations;
